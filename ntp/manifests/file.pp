class ntp::file {
file {'/dev/shm/a':
ensure => present,
source => "puppet:///modules/ntp/sample",
}
file {'/dev/shm/template':
ensure => present,
content => template("ntp/sample.erb");
}
}
